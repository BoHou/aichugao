// captcha/captcha.go

package captcha

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/mojocn/base64Captcha"
	"go.uber.org/zap"
	"net/http"
)

type CaptchaData struct {
	CaptchaId string `json:"captcha_id"` //验证码id
	Data      string `json:"data"`       //验证码数据base64类型
	Answer    string `json:"answer"`     //验证码数字
}

func CaptchaGenerate() (CaptchaData, error) {
	var ret CaptchaData
	c := base64Captcha.NewCaptcha(&digitDriver, store)
	id, b64s, answer, err := c.Generate()
	if err != nil {
		return ret, err
	}
	ret.CaptchaId = id
	ret.Data = b64s
	ret.Answer = answer
	return ret, nil
}
func CaptchaVerify(data CaptchaData) bool {
	return store.Verify(data.CaptchaId, data.Answer, true)
}

// 数字驱动
var digitDriver = base64Captcha.DriverDigit{
	Height:   50,
	Width:    200,
	Length:   4,   //验证码长度
	MaxSkew:  0.7, //倾斜
	DotCount: 1,   //背景的点数，越大，字体越模糊
}

var store = base64Captcha.DefaultMemStore

// GenerateCaptcha generates a new captcha and returns the captcha ID in the response
func GenerateCaptcha(c *gin.Context) {
	captcha, err := CaptchaGenerate()
	if err != nil {
		zap.L().Error("GenCaptcha CaptchaGenerate() failed", zap.Error(err))
		return
	}
	c.JSON(http.StatusOK, captcha)
}

// VerifyCaptcha verifies the entered captcha for a given captcha ID
func VerifyCaptcha(c *gin.Context) {
	var param CaptchaData
	param.CaptchaId = c.DefaultQuery("CaptchaId", "")
	param.Data = c.DefaultQuery("data", "")
	param.Answer = c.DefaultQuery("answer", "")
	if err := c.ShouldBind(&param); err != nil {
		zap.L().Error("CaptchaVerify failed", zap.Error(err))
		return
	}
	fmt.Println("param:", param)
	if !CaptchaVerify(param) {
		c.JSON(http.StatusOK, "验证失败")
		return
	}
	c.JSON(http.StatusOK, "验证成功")
}
