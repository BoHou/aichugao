-- 创建数据库
CREATE DATABASE IF NOT EXISTS common_service;

-- 使用数据库
USE common_service;

-- 创建用户表
CREATE TABLE IF NOT EXISTS user (
                                    userId INT PRIMARY KEY,
                                    userName VARCHAR(255) UNIQUE,
    roleId INT,
    phoneNumber VARCHAR(15) UNIQUE,
    email VARCHAR(255) UNIQUE,
    sex TINYINT(1),
    createTime TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updateTime TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PASSWORD VARCHAR(255),
    FOREIGN KEY (roleId) REFERENCES role(roleId)
    );

-- 创建角色表
CREATE TABLE IF NOT EXISTS role (
                                    roleId INT PRIMARY KEY,
                                    roleName VARCHAR(255)
    );

-- 创建权限表
CREATE TABLE IF NOT EXISTS authority (
                                         menuId INT PRIMARY KEY,
                                         menuName VARCHAR(255),
    menuType VARCHAR(50),
    roleId INT,
    FOREIGN KEY (roleId) REFERENCES role(roleId)
    );
