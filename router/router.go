// router.go

package router

import (
	"aichugao/captcha"
	"aichugao/login"
	"aichugao/registration"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

// Routes 注册路由
func Routes(r *gin.Engine, db *gorm.DB) {
	r.POST("/register", func(c *gin.Context) {
		registration.RegisterUser(c, db)
	})
	r.POST("/login", func(c *gin.Context) {
		login.LoginUser(c, db)
	})
	r.GET("/generateCaptcha", captcha.GenerateCaptcha)
	r.GET("/verifyCaptcha", captcha.VerifyCaptcha)
}
