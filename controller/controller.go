// controller.go

package controller

import (
	"aichugao/models"
	"aichugao/router"
	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/cookie"
	"github.com/gin-gonic/gin"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

// InitializeServer 初始化服务
func InitializeServer() {
	// 初始化数据库连接
	db, err := models.InitializeDatabase()
	if err != nil {
		panic("Error initializing database: " + err.Error())
	}

	// 设置Gin框架
	r := gin.Default()

	// 设置Session中间件
	store := cookie.NewStore([]byte("secret"))
	r.Use(sessions.Sessions("mysession", store))

	// 注册路由
	router.Routes(r, db)

	// 启动服务
	r.Run(":8080")
}
