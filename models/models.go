// models.go

package models

import (
	"github.com/jinzhu/gorm"
	"time"
)

// User 模型
type User struct {
	UserID      int       `gorm:"primary_key;auto_increment" json:"userId"`
	UserName    string    `gorm:"unique;not null" json:"userName"`
	RoleID      int       `json:"roleId"`
	PhoneNumber string    `gorm:"unique;not null" json:"phoneNumber"`
	Email       string    `gorm:"unique;not null" json:"email"`
	Sex         int       `json:"sex"`
	CreateTime  time.Time `gorm:"autoCreateTime"`
	UpdateTime  time.Time `gorm:"autoUpdateTime"`
	Password    string    `json:"password"`
}

// Role 模型
type Role struct {
	RoleID   int    `gorm:"primary_key" json:"roleId"`
	RoleName string `json:"roleName"`
}

// Authority 模型
type Authority struct {
	MenuID   int    `gorm:"primary_key" json:"menuId"`
	MenuName string `json:"menuName"`
	MenuType string `json:"menuType"`
	RoleID   int    `json:"roleId"`
}

// InitializeDatabase 初始化数据库连接
func InitializeDatabase() (*gorm.DB, error) {
	// 连接数据库
	db, err := gorm.Open("mysql", "root:1234@tcp(127.0.0.1:3306)/common_service?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		return nil, err
	}

	// 自动迁移数据库结构
	db.AutoMigrate(&User{}, &Role{}, &Authority{})
	return db, nil
}
