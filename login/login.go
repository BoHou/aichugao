// login/login.go

package login

import (
	"aichugao/models"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"net/http"
)

// LoginRequest 请求JSON结构
type LoginRequest struct {
	UserName string `json:"userName"`
	Password string `json:"password"`
}

// LoginUser 用户登录逻辑
func LoginUser(c *gin.Context, db *gorm.DB) {
	var request LoginRequest
	if err := c.ShouldBindJSON(&request); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid JSON format"})
		return
	}

	session := sessions.Default(c)

	// Find the user by username
	var user models.User
	if err := db.Where("user_name = ? AND password = ?", request.UserName, request.Password).First(&user).Error; err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Invalid credentials"})
		return
	}

	// Set session for the logged-in user (you may want to include more user info in the session)
	session.Set("userId", user.UserID)
	session.Save()

	c.JSON(http.StatusOK, gin.H{"message": "Login successful"})
}
