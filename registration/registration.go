// registration.go

package registration

import (
	"aichugao/models"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"net/http"
	"regexp"
	"time"
)

// RegisterRequest 请求JSON结构
type RegisterRequest struct {
	UserName    string `json:"userName"`
	PhoneNumber string `json:"phoneNumber"`
	Email       string `json:"email"`
	Password    string `json:"password"`
	Sex         int    `json:"sex"`
}

// RegisterUser 注册用户逻辑
func RegisterUser(c *gin.Context, db *gorm.DB) {
	// 获取JSON数据
	var request RegisterRequest
	if err := c.ShouldBindJSON(&request); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid JSON format"})
		return
	}

	if !CheckMobile(request.PhoneNumber) {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid Phone number"})
		return
	}

	if !VerifyEmailFormat(request.Email) {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid email"})
		return
	}

	// 创建用户
	user := models.User{
		UserName:    request.UserName,
		PhoneNumber: request.PhoneNumber,
		Email:       request.Email,
		Password:    request.Password,
		Sex:         request.Sex,
		CreateTime:  time.Now(),
		UpdateTime:  time.Now(),
	}

	db.Where("user_name=?", user.UserName).First(&user)
	if user.UserID > 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "userName exists"})
		return
	}

	db.Where("phone_number=?", user.PhoneNumber).First(&user)
	if user.UserID > 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "phone number exists"})
		return
	}

	db.Where("email=?", user.Email).First(&user)
	if user.UserID > 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "email exists"})
		return
	}
	// 保存用户到数据库
	if err := db.Create(&user).Error; err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to register user"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "User registered successfully"})
}

// CheckMobile Mobile phone number verify
func CheckMobile(phone string) bool {
	regRuler := "1[3456789]{1}\\d{9}$"
	reg := regexp.MustCompile(regRuler)
	return reg.MatchString(phone)
}

// VerifyEmailFormat email verify
func VerifyEmailFormat(email string) bool {
	//pattern := `\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*` //匹配电子邮箱
	pattern := `^[0-9a-z][_.0-9a-z-]{0,31}@([0-9a-z][0-9a-z-]{0,30}[0-9a-z]\.){1,4}[a-z]{2,4}$`

	reg := regexp.MustCompile(pattern)
	return reg.MatchString(email)
}
