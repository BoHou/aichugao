package config

import (
	"fmt"
	"gopkg.in/yaml.v2"
	"os"
)

type Config struct {
	Host         string `yaml:"config.database.host"`
	Port         int    `yaml:"config.database.port"`
	DatabaseName string `yaml:"config.database.database_name"`
	UserName     string `yaml:"config.database.username"`
	PassWord     string `yaml:"config.database.password"`
	CharSet      string `yaml:"config.database.charset"`
}

var config *Config

func init() {
	// 加载配置
	err := load("config/config.yaml")
	if err != nil {
		fmt.Println("Failed to load configuration")
		return
	}
}

func load(path string) error {
	result, err := os.ReadFile(path)
	if err != nil {
		return err
	}

	return yaml.Unmarshal(result, &config)
}

func Get() *Config {
	return config
}
